from flask import Blueprint, render_template, request, session, redirect, url_for
from models.models import shippingAddr, engine, deliveryAddr, order, detailOrder, product, deliverysystem
import json
from vietnam_provinces.enums import ProvinceEnum, DistrictEnum
from vietnam_provinces.enums.wards import WardEnum


userpage__blueprint = Blueprint('userpage', __name__, static_folder = 'static', template_folder = 'templates')

# Route này route cơ bản để khởi tạo
@userpage__blueprint.route("/")
def baseUserpage():
    if 'userName' not in session:
        return redirect(url_for('signin.signin_account', roleUser = 'User'))
    roleUser = session['userName'].split("_")[0]
    if roleUser == 'User':
        return render_template('baseUserPage.html')
    else:
        return "<p>You are not login at role user, so you can't access this page</p>"

# Route này dùng để cho chức năng gửi sản phẩm của người sử dụng, khi tạo đơn hàng cần lưu và 5 bảng, lưu thông tin người nhận hàng vào deliveryaddr,
# thông tin người gửi hàng vào shipping addr, thông tin đơn hàng vào order, thông tin chi tiết đơn hàng vào detailorder, các sản phẩm vào product
@userpage__blueprint.route("/sendProduct", methods=['GET', 'POST'])
def userSendProduct():
    if 'userName' not in session:
        return redirect(url_for('signin.signin_account', roleUser = 'User'))
    roleUser = session['userName'].split("_")[0]
    if roleUser == 'User':
        division_province = []
        conn = engine.connect()
        all_DS = conn.execute(deliverysystem.select().where(deliverysystem.c.status == 'OK')).fetchall()

        with open('userpage__blueprint/static/json/division.json', encoding ='utf8') as division__json:
            division_province = json.load(division__json)
        
        if request.method == 'POST':
            nameSender = request.form['inputSenderName']
            telNumSender = request.form['inputSenderTelNum']
            codeProvinceAddrSender = request.form['chooseAddProvinceSender'].split(",")[1]
            provinceAddrSender = ProvinceEnum[f'P_{codeProvinceAddrSender}'].value.name
            codeDistrictAddrSender = request.form['chooseAddDistrictSender'].split(",")[1]
            districtAddrSender = DistrictEnum[f'D_{codeDistrictAddrSender}'].value.name
            codeWardAddrSender = request.form['chooseAddWardSender'].split(",")[1]
            wardAddrSender = WardEnum[f'W_{codeWardAddrSender}'].value.name
            typeAddrSender = request.form['typeAddressSender']
            noteAddrSender = request.form['noteAddressSender']

            allProductAdd = int(request.form['allProductAdd'])
            lstAllProduct = []
            totalCostAllProduct = 0
            for index in range(0, allProductAdd):
                # each list containt : productName, typeProduct, quantityProduct, weightProduct, sizeProduct, descriptionProduct     
                lstAllProduct.append([request.form[f'productName{index}'], request.form[f'chooseTypeProduct{index}'],
                    request.form[f'productQuantity{index}'], request.form[f'productWeight{index}'],
                    request.form[f'productSize{index}'], request.form[f'descriptionProduct{index}']])
                totalCostAllProduct = totalCostAllProduct + sum([int(i) for i in request.form[f'productSize{index}'].split("x")]) * 1000

            nameReceiver = request.form['inputReceiverName']
            telNumReceiver = request.form['inputReceiverTelNum']
            codeProvinceAddrReceiver = request.form['chooseAddProvinceReceiver'].split(",")[1]
            provinceAddrReceiver = ProvinceEnum[f'P_{codeProvinceAddrReceiver}'].value.name
            codeDistrictAddrReceiver = request.form['chooseAddDistrictReceiver'].split(",")[1]
            districtAddrReceiver = DistrictEnum[f'D_{codeDistrictAddrReceiver}'].value.name
            codeWardAddrReceiver = request.form['chooseAddWardReceiver'].split(",")[1]
            wardAddrReceiver = WardEnum[f'W_{codeWardAddrReceiver}'].value.name
            typeAddrReceiver = request.form['typeAddressReceiver']
            noteAddrReceiver = request.form['noteAddrReceiver']

            conn = engine.connect()
            idShippingAddr = len(conn.execute(shippingAddr.select()).fetchall()) + 1
            idDeliveryAddr = len(conn.execute(deliveryAddr.select()).fetchall()) + 1
            idDetailOrder = len(conn.execute(detailOrder.select()).fetchall()) + 1

            insShippingAddr = shippingAddr.insert().values(
                idShippingAddr = idShippingAddr,
                idUser = session['userName'].split('_')[-1],
                name = nameSender,
                telNum = telNumSender,
                country = provinceAddrSender,
                district = districtAddrSender,
                ward = wardAddrSender,
                type = typeAddrSender,
                note = noteAddrSender,
                status = 'Pending',
                latitude = '',
                longitude = ''
            )

            insDeliveryAddr = deliveryAddr.insert().values(
                idDeliveryAddr = idDeliveryAddr,
                idUser = session['userName'].split('_')[-1],
                country = provinceAddrReceiver,
                name = nameReceiver,
                telNum = telNumReceiver,
                district = districtAddrReceiver,
                ward = wardAddrReceiver,
                type = typeAddrReceiver,
                note = noteAddrReceiver,
                status = 'Pending',
                latitude = '',
                longitude = ''
            )

            import datetime
            orderCodeRandom = datetime.datetime.now().strftime(f"%Y%m%d%H%M%S") + session['userName'].split('_')[0] + session['userName'].split('_')[1] + telNumSender + telNumReceiver

            insOrder = order.insert().values(
                idUser = session['userName'].split('_')[-1],
                codeOrder = orderCodeRandom
            )

            insDetailOrder = detailOrder.insert().values(
                codeOrder = orderCodeRandom,
                idDeliveryAddr =idDeliveryAddr,
                idShippingAddr = idShippingAddr,
                idDriver = None,
                dateCreate = datetime.datetime.now().strftime(f"%Y-%m-%d %H:%M:%S"),
                departureTime = (datetime.datetime.now() + datetime.timedelta(hours=1)).strftime(f"%Y-%m-%d %H:%M:%S"),
                estimateArrive = (datetime.datetime.now() + datetime.timedelta(hours=100)).strftime(f"%Y-%m-%d %H:%M:%S"),
                totalCost = str(totalCostAllProduct),
                note = None,
                status = 'Pending',
                idDeliverySystem = request.form['chooseDS'],
                realTimeArrive = None
            )

            conn.execute(insShippingAddr)
            conn.execute(insDeliveryAddr)
            conn.execute(insOrder)
            conn.execute(insDetailOrder)

            idDetailOrder = conn.execute(detailOrder.select().where(detailOrder.c.codeOrder == orderCodeRandom)).fetchone()[0]

            print(len(conn.execute(detailOrder.select()).fetchall()))
            print(idDetailOrder)
            # each list containt : productName, typeProduct, quantityProduct, weightProduct, sizeProduct, descriptionProduct
            for index in lstAllProduct:
                insProduct = product.insert().values(
                    idDetailOrder = idDetailOrder,
                    name = index[0],
                    description = index[5],
                    quantity = index[2],
                    weight = index[3],
                    type = index[1],
                    size = index[4],
                    cost = str(sum([int(i) for i in index[4].split("x")]) * 1000)
                )
                conn.execute(insProduct)
            return render_template('userSendProduct.html', division_province = division_province, all_DS = all_DS)
        return render_template('userSendProduct.html', division_province = division_province, all_DS = all_DS)
    else:
        return "<p>You are not login at role user, so you can't access this page</p>"

# Đăng xuất
@userpage__blueprint.route('/logout')
def userPageLogout():
    session.clear()
    return redirect(url_for('homepagePage.homepage'))

